import React from "react";
import { Admin, Resource, ListGuesser, EditGuesser } from "react-admin";
import simpleRestProvider from "ra-data-simple-rest";
import { AppliqueList } from "./store/applique/AppliqueList";
import { AppliqueEdit } from "./store/applique/AppliqueEdit";
import { AppliqueCreate } from "./store/applique/AppliqueCreate";
import { PaperList } from "./store/backingPaper/PaperList";
import { PaperEdit } from "./store/backingPaper/PaperEdit";
import { PaperCreate } from "./store/backingPaper/PaperCreate";
import { MachineList } from "./store/machine/MachineList";
import { MachineEdit } from "./store/machine/MachineEdit";
import { MachineCreate } from "./store/machine/MachineCreate";
import { SequinCreate } from "./store/sequin/SequinCreate";
import { SequinEdit } from "./store/sequin/SequinEdit";
import { SequinList } from "./store/sequin/SequinList";
import { ThreadList } from "./store/thread/ThreadList";
import { ThreadEdit } from "./store/thread/ThreadEdit";
import { ThreadCreate } from "./store/thread/ThreadCreate";

const dataProvider = simpleRestProvider("http://localhost:4000/store");
const App = () => (
  <Admin dataProvider={dataProvider}>
    <Resource
      name="appliques"
      list={AppliqueList}
      create={AppliqueCreate}
      edit={AppliqueEdit}
    />
    <Resource
      name="papers"
      list={PaperList}
      edit={PaperEdit}
      create={PaperCreate}
    />
    <Resource
      name="machines"
      list={MachineList}
      edit={MachineEdit}
      create={MachineCreate}
    />
    <Resource
      name="sequins"
      list={SequinList}
      edit={SequinEdit}
      create={SequinCreate}
    />

    <Resource
      name="threads"
      list={ThreadList}
      edit={ThreadEdit}
      create={ThreadCreate}
    />
  </Admin>
);

export default App;
