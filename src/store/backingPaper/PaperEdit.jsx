import React from 'react'
import {SimpleForm, TextInput, Edit,NumberInput,DisabledInput} from 'react-admin'
export const PaperEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="quality" />
            <NumberInput source="size" />
            <TextInput source="type" />
            <DisabledInput source="created_at" />
            <DisabledInput source="updated_at" />
            <DisabledInput source="created_by" />
            <DisabledInput source="updated_by" />
        </SimpleForm>
    </Edit>

);