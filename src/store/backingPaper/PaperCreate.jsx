import React from 'react'
import {SimpleForm, TextInput, Create,NumberInput,DisabledInput} from 'react-admin'
export const PaperCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="quality" />
            <NumberInput source="size" />
            <TextInput source="type" />
            <DisabledInput source="created_at" />
            <DisabledInput source="updated_at" />
            <DisabledInput source="created_by" />
            <DisabledInput source="updated_by" />
        </SimpleForm>
    </Create>

);