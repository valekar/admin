import React from 'react'
import { Datagrid, TextField,BooleanField,List,NumberField,DateField } from 'react-admin';
export const PaperList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="quality" />
            <NumberField source="size" />
            <TextField source="type" />
            <BooleanField source="active" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
            <TextField source="created_by" />
            <TextField source="updated_by" />
        </Datagrid>
    </List>
);