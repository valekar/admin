import React from 'react'
import {SimpleForm, TextInput, BooleanInput, Create,NumberInput,DisabledInput} from 'react-admin'
export const MachineCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <NumberInput source="heads" />
            <TextInput source="type" />
            <NumberInput source="stitchCapacity" />
            <BooleanInput source="active" />
            <DisabledInput source="created_at" />
            <DisabledInput source="updated_at" />
            <DisabledInput source="created_by" />
            <DisabledInput source="updated_by" />
        </SimpleForm>
    </Create>
);