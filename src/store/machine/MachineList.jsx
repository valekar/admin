import React from 'react'
import { Datagrid, TextField,BooleanField,List,NumberField,DateField } from 'react-admin';
export const MachineList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="name" />
            <NumberField source="heads" />
            <TextField source="type" />
            <NumberField source="stitchCapacity" />
            <BooleanField source="active" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
            <TextField source="created_by" />
            <TextField source="updated_by" />
        </Datagrid>
    </List>
);