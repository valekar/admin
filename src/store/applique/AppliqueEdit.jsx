import React from 'react'
import {SimpleForm, TextInput, Edit,NumberInput,DisabledInput} from 'react-admin'

export const AppliqueEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <NumberInput source="number" />
            <TextInput source="type" />
            <TextInput source="color" />
            <DisabledInput source="created_at" />
            <DisabledInput source="updated_at" />
            <DisabledInput source="created_by" />
            <DisabledInput source="updated_by" />
        </SimpleForm>
    </Edit>
);