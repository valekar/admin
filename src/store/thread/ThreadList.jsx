import React from "react";
import {
  Datagrid,
  TextField,
  BooleanField,
  List,
  DateField
} from "react-admin";

export const ThreadList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="brandName" />
      <TextField source="color" />
      <TextField source="type" />
      <BooleanField source="active" />
      <DateField source="created_at" />
      <DateField source="updated_at" />
      <TextField source="created_by" />
      <TextField source="updated_by" />
    </Datagrid>
  </List>
);
