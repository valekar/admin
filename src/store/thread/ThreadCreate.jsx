import React from "react";
import {
  SimpleForm,
  TextInput,
  BooleanInput,
  DisabledInput,
  Create
} from "react-admin";

export const ThreadCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="brandName" />
      <TextInput source="color" />
      <TextInput source="type" />
      <BooleanInput source="active" />
      <DisabledInput source="created_at" />
      <DisabledInput source="updated_at" />
      <DisabledInput source="created_by" />
      <DisabledInput source="updated_by" />
    </SimpleForm>
  </Create>
);
