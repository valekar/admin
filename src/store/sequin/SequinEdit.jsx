import React from "react";
import {
  SimpleForm,
  TextInput,
  BooleanInput,
  DisabledInput,
  Edit,
  NumberInput
} from "react-admin";
export const SequinEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <NumberInput source="size" />
      <TextInput source="color" />
      <BooleanInput source="active" />
      <DisabledInput source="created_at" />
      <DisabledInput source="updated_at" />
      <DisabledInput source="created_by" />
      <DisabledInput source="updated_by" />
    </SimpleForm>
  </Edit>
);
