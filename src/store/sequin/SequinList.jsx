import React from 'react'
import { Datagrid, TextField,BooleanField,List,NumberField,DateField } from 'react-admin';
export const SequinList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <NumberField source="size" />
            <TextField source="color" />
            <BooleanField source="active" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
            <TextField source="created_by" />
            <TextField source="updated_by" />
        </Datagrid>
    </List>
);