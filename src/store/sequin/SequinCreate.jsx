import React from "react";
import {
  SimpleForm,
  TextInput,
  BooleanInput,
  DisabledInput,
  Create,
  NumberInput
} from "react-admin";

export const SequinCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <NumberInput source="size" />
      <TextInput source="color" />
      <BooleanInput source="active" />
      <DisabledInput source="created_at" />
      <DisabledInput source="updated_at" />
      <DisabledInput source="created_by" />
      <DisabledInput source="updated_by" />
    </SimpleForm>
  </Create>
);
